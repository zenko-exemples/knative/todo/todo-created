use envconfig::Envconfig;
use log::LevelFilter;

#[derive(Envconfig, Clone)]
pub struct Config {
    #[envconfig(from = "PORT", default = "8080")]
    pub port: u16,

    #[envconfig(from = "HOST", default = "127.0.0.1")]
    pub host: String,

    #[envconfig(nested = true)]
    pub log: Log,

    #[envconfig(
        from = "BEECEPTOR_URI",
        default = "https://zenko-test.free.beeceptor.com"
    )]
    pub beeceptor_uri: String,
}

#[derive(Envconfig, Clone)]
pub struct Log {
    #[envconfig(from = "WARP_LOG", default = "info")]
    pub warp: LevelFilter,

    #[envconfig(from = "HANDLER_LOG", default = "info")]
    pub handler: LevelFilter,
}
