use cloudevents::{AttributesReader, Event};
use std::convert::Infallible;
use warp::hyper::StatusCode;

use crate::{config::Config, messages::Message};

pub async fn handle_event(event: Event, config: Config) -> Result<impl warp::Reply, Infallible> {
    log::debug!(target: "handler", "Event {:?} Received", &event.ty());

    let data: Message = match event.data() {
        Some(data) => match data.clone().try_into() {
            Ok(data) => data,
            Err(error) => {
                log::error!(target: "handler", "\t->Cannot parse message: {:?}", error);
                return Ok(StatusCode::BAD_REQUEST);
            }
        },
        None => {
            log::error!(target: "handler", "\t->No message given");
            return Ok(StatusCode::BAD_REQUEST);
        }
    };

    log::debug!(target: "handler", "\t-> Data: {:?}", data);

    let client = reqwest::Client::new();
    let res = client.post(config.beeceptor_uri).json(&data).send().await;

    match res {
        Ok(_) => Ok(StatusCode::NO_CONTENT),
        Err(error) => {
            log::error!(target: "handler", "\t-> Error when sending the event to a external service: {:?}", error);
            return Ok(StatusCode::INTERNAL_SERVER_ERROR);
        }
    }
}
