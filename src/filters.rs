use cloudevents::binding::warp::filter;
use warp::Filter;

use crate::{config::Config, handlers};

pub fn main(
    config: Config,
) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
    event(config.clone())
}

pub fn event(
    config: Config,
) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
    warp::post()
        .and(filter::to_event())
        .and(with_config(config))
        .and_then(handlers::handle_event)
}

fn with_config(
    config: Config,
) -> impl Filter<Extract = (Config,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || config.clone())
}
