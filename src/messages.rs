use cloudevents::Data;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Message {
    id: String,
    title: String,
    description: Option<String>,
}

impl TryFrom<Data> for Message {
    type Error = ();

    fn try_from(value: Data) -> Result<Self, Self::Error> {
        let serde_value: serde_json::Value = match value.try_into() {
            Ok(serde_value) => serde_value,
            Err(_) => return Err(()),
        };
        match serde_json::from_value(serde_value) {
            Ok(data) => Ok(data),
            Err(_) => Err(()),
        }
    }
}
