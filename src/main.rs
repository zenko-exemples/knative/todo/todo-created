mod config;
mod filters;
mod handlers;
mod messages;

use envconfig::Envconfig;
use pretty_env_logger::env_logger::Builder;
use std::net::Ipv4Addr;
use warp::Filter;

use config::Config;

#[tokio::main]
async fn main() {
    let config = Config::init_from_env().unwrap();

    Builder::new()
        .filter(Some("warp"), config.log.warp)
        .filter(Some("handler"), config.log.handler)
        .init();

    let api = filters::main(config.clone());

    let routes = api.with(warp::log("warp"));

    let host: Ipv4Addr = config.host.parse().unwrap();
    let port: u16 = config.port;

    warp::serve(routes).run((host, port)).await;
}
